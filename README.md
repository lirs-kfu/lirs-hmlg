# Laboratory of Intelligent Robotic Systems - Human Models Library for Gazebo



## Getting started

example of using the model

```
<actor name="c_casual_talk">
  	<pose>
    	10 -10 0 0 0 1.57
  	</pose>

  	<skin>
            <filename>model://models/actors/Male/c_casual/talk.dae</filename>
            <scale>1</scale>
            <pose>
    	10 -10 0 0 0 1.57
  	</pose>
        </skin>
        
        <link name="link">
    <collision name="box">
    <pose>0 0 0 2.61 3.14 0</pose>
      <geometry>
        <box>
              <size>0.6 1.5 0.6</size>
            </box>
      </geometry>
    </collision>
    <visual name="visual">
      <pose>0 0 0 0 0 0</pose>
      <geometry>
        <box>
              <size>0.0001 0.0001 0.0001</size>
            </box>
      </geometry>
    </visual>
  </link>
  <script>
          <loop>true</loop>
          <delay_start>0.000000</delay_start>
          <auto_start>true</auto_start>
          
          <trajectory id="6" type="walk1">
            <waypoint>
              <time>0</time>
              <pose>-2 -8 0 0 0 1.57</pose>
            </waypoint>	

            <waypoint>
              <time>40</time>
              <pose>-2 -8 0 0 0 1.57</pose>
            </waypoint>		
	        </trajectory>
        </script>
</actor>
```
